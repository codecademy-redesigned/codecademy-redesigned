# Codecademy Redesigned

This repository has been created for the following reasons: 

1. To bring the code for the Chrome and Firefox versions together under one repository
2. To make goals, ideas, and issues easier to track
3. And to make it easier for anyone to contribute to the addon. 

Feel free to submit a merge request editing or deleting anything. 

## Resources

- https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Your_first_WebExtension
- https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext
- https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Anatomy_of_a_WebExtension
- https://developer.chrome.com/extensions/getstarted
- https://developer.chrome.com/extensions/overview