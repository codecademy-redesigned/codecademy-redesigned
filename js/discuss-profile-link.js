// go down an extra level and traverse back up because there are multiple
// <h3>s on the page, but no others with a map marker icon in them
var parent = document.querySelector('.primary-textual h3 .fa-map-marker'),
    username = document.querySelector('.username').textContent;

// traverse back up
parent = parent.parentElement;
// get rid of the accessibility text
username = username.split(" ")[0];

// create the link to the main site profile
var icon = document.createElement("i"),
    spacer = document.createElement("span"),
    link = document.createElement("a");

icon.classList.add("fa", "fa-external-link");
spacer.textContent = " ";
link.href = `https://www.codecademy.com/${username}`;
link.textContent = "Codecademy"

parent.appendChild(icon);
parent.appendChild(spacer);
parent.appendChild(link);