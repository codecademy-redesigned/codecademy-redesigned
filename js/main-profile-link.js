if (document.querySelector('.profiles') !== null) {
  var username = location.pathname;
  // remove `/`s from the username
  username = username.replace(/\W/g, "");

  // go down for specificity, then traverse up to correct element
  var parent = document.querySelector('.avatar');
  parent = parent.parentElement;

  // add the link to the Discuss profile
  var nameEl = parent.querySelector('h3')
      link = document.createElement("a");

  link.style["font-size"] = "initial";
  link.href = `https://discuss.codecademy.com/users/${username}`;
  link.textContent = `↗ @${username}`;

  nameEl.appendChild(link);
}